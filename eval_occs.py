#########################################
# Useful functions for evaluation:
# (1) Confusion matrix
# (2) Histograms on errors
#########################################


# (1) Confusion matrix (using sklearn):

from sklearn import metrics

# given 
# trained classifier: clf
# test set: [x_test,y_test] 
# names for each class: y_int_term (list with e.g. 'Taxiförare' for class i)
# 

predicted = clf.predict(x_test)

rep = metrics.classification_report(y_test, predicted, target_names = y_int_term, output_dict = True)
print(rep)

# or 
conf_matrix = metrics.confusion_matrix(y_test,predicted)
print(conf_matrix)

# and do some sort:
res = list(rep.items())[0:len(rep)-3]
res = sorted(res, key=lambda x: x[1]['precision'], reverse=True)
print(res)



# (2) Histograms on errors

# To backtrack the errors, we can check the real classes in the test set and sum up the results.
# either for a single one:
c = y_int_term.index('Brandingenjörer och byggnadsinspektörer m.fl.; 3355')
indexes = [i for i,d in enumerate(y_test) if d == c]
y_vals = Counter([y_test[index] for index in indexes]).most_common()
# 
# or for all classes - this will give a good visual understanding of how the occupations classification errors will look:
#
# e.g.  ------------> real class
#       |
#       |       
#       |
#      \/
#    classified


# which inputs that are expected to cause a classification can also be looked at (if the input features are kept simple - by e.g. only using enriched data as input):  
x_vals = Counter([x_test[index] for index in indexes]).most_common()
# 
# results for the above for only enriched occupations:
# x_vals = [('Brandingenjör', 180), ('Byggnadsinspektör', 97), ('Bygglovshandläggare', 86), ('Brandskyddstekniker', 39), ('Byggnadsinspektör By...andläggare', 38), ('Bygglovshandläggare ...sinspektör', 19), ('Brandtekniker', 19), ('Tekniker Brandskyddstekniker', 18), ('Fukttekniker', 18), ('Brandinspektör', 14), ('Brandskyddstekniker Tekniker', 13), ('Projektledare', 12), ('Energispecialist', 12), ('Energirådgivare Klim...trådgivare', 12), ...] 
