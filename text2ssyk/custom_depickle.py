# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#
# ADDITIONAL INFORMATION ABOUT CODE COPIED FROM THE JOBLIB LIBRARY:
#
# The code in this file is a modification of the source code from numpy_pickle.py 
# of the Joblib library (see https://github.com/joblib/joblib/blob/master/joblib/numpy_pickle.py)
# which is licensed under BSD-3:
#
#                   BSD 3-Clause License
#
#                   Copyright (c) 2008-2021, The joblib developers.
#                   All rights reserved.
#
#                   Redistribution and use in source and binary forms, with or without
#                   modification, are permitted provided that the following conditions are met:
#
#                   * Redistributions of source code must retain the above copyright notice, this
#                     list of conditions and the following disclaimer.
#
#                   * Redistributions in binary form must reproduce the above copyright notice,
#                     this list of conditions and the following disclaimer in the documentation
#                     and/or other materials provided with the distribution.
#
#                   * Neither the name of the copyright holder nor the names of its
#                     contributors may be used to endorse or promote products derived from
#                     this software without specific prior written permission.
#
#                   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#                   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#                   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#                   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#                   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#                   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#                   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#                   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#                   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#                   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from pathlib import Path
from joblib.numpy_pickle_utils import _read_fileobject
from joblib.numpy_pickle_compat import load_compatibility
import joblib.numpy_pickle as numpy_pickle

class BackwardCompatibleUnpickler(numpy_pickle.NumpyUnpickler):
    def __init__(self, filename, fobj, mmap_mode=None):
        super().__init__(filename, fobj, mmap_mode)
        self.moduleRemapping = {"sklearn.ensemble.forest": "sklearn.ensemble._forest",
                                "sklearn.tree.tree": "sklearn.tree",
                                "sklearn.externals.joblib": "joblib",
                                "sklearn.externals.joblib.numpy_pickle": "joblib.numpy_pickle"}
        self.isDebug = False

    def remap_module(self, module):
        remapped = self.moduleRemapping.get(module, module)
        if self.isDebug and remapped != module:
            print("  * Remap module")
        return remapped

    def find_class(self, module, name):            
        if self.isDebug:
            print("find_class('{:s}', {:s}')".format(module, name))
            
        module = self.remap_module(module)

        if self.isDebug:
            print("  -> super().find_class('{:s}', '{:s}')".format(module, name))
        
        return super().find_class(module, name)
        

def _unpickle(fobj, filename="", mmap_mode=None):
    """Internal unpickling function."""
    # We are careful to open the file handle early and keep it open to
    # avoid race-conditions on renames.
    # That said, if data is stored in companion files, which can be
    # the case with the old persistence format, moving the directory
    # will create a race when joblib tries to access the companion
    # files.
    unpickler = BackwardCompatibleUnpickler(filename, fobj, mmap_mode=mmap_mode)
    obj = None
    try:
        obj = unpickler.load()
        if unpickler.compat_mode:
            warnings.warn("The file '%s' has been generated with a "
                          "joblib version less than 0.10. "
                          "Please regenerate this pickle file."
                          % filename,
                          DeprecationWarning, stacklevel=3)
    except UnicodeDecodeError as exc:
        # More user-friendly error message
        new_exc = ValueError(
            'You may be trying to read with '
            'python 3 a joblib pickle generated with python 2. '
            'This feature is not supported by joblib.')
        new_exc.__cause__ = exc
        raise new_exc
    return obj

def load(filename, mmap_mode=None):
    if Path is not None and isinstance(filename, Path):
        filename = str(filename)

    if hasattr(filename, "read"):
        fobj = filename
        filename = getattr(fobj, 'name', '')
        with _read_fileobject(fobj, filename, mmap_mode) as fobj:
            obj = _unpickle(fobj)
    else:
        with open(filename, 'rb') as f:
            with _read_fileobject(f, filename, mmap_mode) as fobj:
                if isinstance(fobj, str):
                    # if the returned file object is a string, this means we
                    # try to load a pickle file generated with an version of
                    # Joblib so we load it with joblib compatibility function.
                    raise RuntimeError("Don't go here")
                    return load_compatibility(fobj)

                obj = _unpickle(fobj, filename, mmap_mode)
    return obj
