# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
import numpy as np
import pickle
import joblib
from text2ssyk import help_functions
import text2ssyk.custom_depickle as cdp
from pathlib import Path

class Text2SsykClassifier:
    """This class classifies jobads to ssyk codes."""
    def __init__(self, vectorizer, tfidconverter, classifier):
        self._vectorizer = vectorizer
        self._tfidconverter = tfidconverter
        self._classifier = classifier

    def class_from_index(self, index):
        return self._classifier.classes_[index]

    def classes_from_indices(self, inds):
        return [self.class_from_index(i) for i in inds]
        
    def classifyTextsTopN(self, inputTexts, n):
        """This method takes a list of texts from jobads and returns a list of lists, where every nested list has the top `n` best ssyk codes for the corresponding job ad."""
        assert(not(isinstance(inputTexts, str)))
        X = [help_functions.preprocessText(s) for s in inputTexts]
        X = self._vectorizer.transform(X)
        X = self._tfidconverter.transform(X)

        rTop_probs = self._classifier.predict_proba(X)
        rTop_inds = np.argsort(rTop_probs)[:,:-n-1:-1]
        
        return [self.classes_from_indices(r) for r in rTop_inds]
        

class Text2SsykConfig:
    def __init__(self, filenameModel, filenameVectorizer, filenameTfid):
        self.filenameModel = filenameModel
        self.filenameVectorizer = filenameVectorizer
        self.filenameTfid = filenameTfid
        self.isDebug = False
        self.topN = 5

    def from_path(pathModels0):
        pathModels = Path(pathModels0)
        files = list(os.listdir(pathModels))
        def find_file(prefix, suffix):
            candidates = [pathModels.joinpath(filename)
                          for filename in files
                          for fname in [filename.lower()]
                          if fname.startswith(prefix) and fname.endswith(suffix)]
            if len(candidates) == 1:
                return candidates[0]
            else:
                raise RuntimeError(f"Failed to resolve filename like '{prefix}...{suffix}'")
        return Text2SsykConfig(find_file("model_", ".gzip"),
                               find_file("vector_", ".pickle"),
                               find_file("tfid_", ".pickle"))

    def load_pickle(self, filename):
        if self.isDebug:
            print("\n*** DEPICKLE: " + filename)
        with open(filename, "rb") as f:
            return pickle.load(f)

    def joblib_load(self, filename):
        if self.isDebug:
            print("\n*** JOBLIB: " + filename)

        # Instead of calling joblib.load(filename), we call our
        # custom unpickling implementation that supports unpickling
        # data from previous versions of libraries.
        return cdp.load(filename)

    def load_classifier(self):
        vectorizer = self.load_pickle(self.filenameVectorizer)
        tfidconverter = self.load_pickle(self.filenameTfid)        
        classifier = self.joblib_load(self.filenameModel)
        return Text2SsykClassifier(vectorizer, tfidconverter, classifier)
    
    def load_and_classify(self, data):
        full_classifier = self.load_classifier()

        if self.isDebug:
            print("\n\n*** Successfully loaded classifier data!\n\n\n")
            print('data:', data)

        if len(data) > 0:
            inputTexts = [d['originalJobPosting']['description'] for d in data]

            resTopN = full_classifier.classifyTextsTopN(inputTexts, self.topN)

            # need to go from numpy to python int to make json serializable
            for i, r in enumerate(resTopN):
                data[i]['ssyk_lvl4'] = int(r[0])

        return data

dev_model_dir = "models/dev/"

def model_dir_from_env():
    return os.environ.get("TEXT2SSYK_ROOT", dev_model_dir)
