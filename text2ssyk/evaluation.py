# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import classify

pathModels = 'models/'
filename_model = pathModels + 'model_random_forest_1000000_15000_200.gzip'#'model_random_forest_50000_15000.gzip'
filename_vectorizer = pathModels + 'vector_1000000_15000.pickle'#'vector_50000_15000.pickle'
filename_tfid = pathModels + 'tfid_1000000_15000.pickle'#'tfid_50000_15000.pickle'

def load_resave_file(filename, file_type = 'json_lines'):

    manual_ids = [10235,46025]
    
    if file_type == 'json_lines':
        with open(filename,encoding="utf8") as f:
            lines = f.readlines()
            data = [json.loads(l) for l in lines]
#            data = json.load(f)
    else:
        with open(filename,encoding="utf8") as f:
            data = json.load(f)

    print('loaded',len(data),'items')

    c = classify.classify()
    print('loading classifier')
    c.load_model_baseline(filename_vectorizer,filename_tfid,filename_model)

    print('setting up input data')
    input_texts = []
    nrIsLists = 0
    for i,d0 in enumerate(data):

        d = d0['originalJobPosting']

        in_text = ''
        if 'title' in d:
            in_text += d['title'] + ' '
        if 'description' in d:
            if isinstance(d['description'], list):
                print('error is list of length',len(d['description']))
                nrIsLists += 1
                in_text += d['description'][0]
            else:
                in_text += d['description']

        input_texts.append(in_text)

    print('nr that were lists and not strs:', nrIsLists)
    print('classifying')
    res_top_5 = c.classify_model_baseline(input_texts)

    short_data = []

    for i,d in enumerate(data):
        legacy_id = int(res_top_5[i][0]) # int32 -> int to make it json serializable
        concept_id = -1

        if d['id'] in manual_ids:
            print(legacy_id)

        d['ssyk'] = legacy_id
        short_data.append({'id': d['id'], 'ssyk': d['ssyk']})

    print('resaving')

    with open('results.json','w') as f:
        json.dump(data,f)
    with open('results_short.json','w') as f:
        json.dump(short_data,f)

#load_resave_file('data/2020-02-14.json')
load_resave_file('data/ads_20200514_hash_sorted.json')
