# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import numpy as np
import pickle

import joblib
#from sklearn.externals import joblib

import os
import sys
import text2ssyk.help_functions

class classify(object):

    def load_model_baseline(self,filename_vec, filename_tfid, filename_model):
        
        self._vectorizer = pickle.load(open(filename_vec, "rb"))
        self._tfidconverter = pickle.load(open(filename_tfid,"rb"))
        self._classifier = joblib.load(filename_model)

    def classify_model_baseline(self,inputTexts):

        X = [help_functions.preprocessText(s) for s in inputTexts]
        X = self._vectorizer.transform(X)
        X = self._tfidconverter.transform(X)

        #res = classifier.predict(X)
        rTop = self._classifier.predict_proba(X)

        n = 5
        rTop = np.argsort(rTop)[:,:-n-1:-1]
        resTop5 = []
        for r in rTop:
            rs = [self._classifier.classes_[i] for i in r]
            resTop5.append(rs)

        return resTop5
