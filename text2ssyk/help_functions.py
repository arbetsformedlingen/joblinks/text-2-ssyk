# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import numpy as np
import re
from pathlib import Path
#from nltk.stem import WordNetLemmatizer

def project_root():
    """This returns the root of this project on the filesystem. It might not work if you use text-2-ssyk as a dependency from another library."""
    root = Path(__file__).parent.parent

    # Check that it seems correct.
    assert(root.joinpath("pyproject.toml").exists())
    
    return root

def preprocessText(s):

    #stemmer = WordNetLemmatizer()

    # the special characters
    sNew = re.sub(r'\W', ' ', str(s))
    
    # single characters
    sNew = re.sub(r'\s+[a-zA-Z]\s+', ' ', sNew)
    
    # single characters from the start
    sNew = re.sub(r'\^[a-zA-Z]\s+', ' ', sNew) 
    
    # multiple spaces with single space
    sNew = re.sub(r'\s+', ' ', sNew, flags=re.I)
    
    # prefixed 'b'
    sNew = re.sub(r'^b\s+', '', sNew)
    
    # Lowercase
    sNew = sNew.lower()
    
    # Lemmatization
    sNew = sNew.split()

    #sNew = [stemmer.lemmatize(word) for word in sNew]
    sNew = ' '.join(sNew)
    
    return sNew


def occId_converts(occ_id):

    ssyk = occ_id

    return { 'id': occ_id, 'id_name': '', 'ssyk': ssyk,  'ssyk_name': '' }
