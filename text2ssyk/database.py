import collections.abc as abc
import sqlite3
from pathlib import Path
import os

class DatasetItem:
    def __init__(self, index, text, label):
        if not(isinstance(text, str)):
            raise RuntimeError(f"Not text: {text} for ({index}, {text}, {label})")
        assert(isinstance(index, int))
        assert(isinstance(text, str))
        assert(isinstance(label, int))
        self.index = index
        self.text = text
        self.label = label

    def __eq__(self, other):
        return (self.index == other.index) and (self.text == other.text) and (self.label == other.label)

    def __repr__(self):
        return f"DatasetItem(index={self.index}, label={self.label})"

    def dict(self):
        return {"index": self.index, "text": self.text, "label": self.label}

class RandomDbAccess:
    def __init__(self, storage):
        self._storage = storage
        self._con = None

    def __enter__(self):
        self._con = self._storage.connection()
        return self

    def __exit__(self, *args):
        self._con.close()
        self._con = None

    def __getitem__(self, jobad_id):
        for (i, text, label) in self._con.execute(f"SELECT * FROM dataset WHERE id={jobad_id}"):
            return DatasetItem(i, text, label)
        
class SqliteStorage:
    def __init__(self, filename):
        self._filename = Path(filename)

    def connection(self):
        os.makedirs(self._filename.parent, exist_ok=True)
        return sqlite3.connect(self._filename)

    def exists(self):
        with self.connection() as con:
            results = list(con.execute(
                f"SELECT name FROM sqlite_master WHERE type='table' AND name='dataset'"))
            return 0 < len(results)

    def __repr__(self):
        return f"SqliteStorage({str(self._filename)})"
    
    def initialize(self):
        if not(self.exists()):
            with self.connection() as con:
                con.execute("CREATE TABLE IF NOT EXISTS dataset(id integer primary key, description text, label integer)")

    def insert_or_update(self, dataset_items):
        self.initialize()
        with self.connection() as con:
            expr = "INSERT OR REPLACE INTO dataset(id, description, label) VALUES(?, ?, ?)"
            data = ((x.index, x.text, x.label) for x in dataset_items)
            con.executemany(expr, data)

    def get_indices(self):
        with self.connection() as con:
            return (i for (i,) in con.execute("SELECT id FROM dataset"))

    def random_access(self):
        return RandomDbAccess(self)

def is_index_vector(X):
    try:
        for x in X:
            i = int(x)
        return True
    except Exception:
        return False

class Dataset(abc.Sequence):
    def __init__(self, items, inds, mapper=None):
        self._items = items
        self._inds = list(inds)
        self._mapper = mapper or (lambda x: x)

    def build(items):
        return Dataset(items, range(len(items)))

    def __len__(self):
        return len(self._inds)

    def __repr__(self):
        return f"Dataset({len(self._inds)})"

    def map(self, f):
        return Dataset(self._items, self._inds, lambda x: f(self._mapper(x)))

    def __getitem__(self, I):
        if is_index_vector(I):
            inds = [self._inds[i] for i in I]
            return Dataset(self._items, inds, self._mapper)
        elif isinstance(I, int):
            return self._mapper(self._items[self._inds[I]])
        else:
            raise RuntimeError(f"__getitem__ not supported for type {type(I)}: {I}")
    
class DatasetHandle:
    def __init__(self, storage, inds):
        self._storage = storage
        self._inds = inds
        self._access = None

    def __enter__(self):
        self._access = self._storage.random_access()
        return Dataset(self._access.__enter__(), self._inds)

    def __exit__(self, *args):
        self._access.__exit__(*args)
        self._access = True

def dataset_labels(dataset):
    return dataset.map(lambda item: item.label)

def dataset_texts(dataset):
    return dataset.map(lambda item: item.text)
        
