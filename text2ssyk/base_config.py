from text2ssyk.data_handling import write_pickle, read_pickle, occ_id_from_ad, read_json, write_json, cached_evaluate
import json
import text2ssyk.help_functions as hf
import os
from text2ssyk.database import SqliteStorage, Dataset, DatasetItem, DatasetHandle, dataset_labels, dataset_texts
import random
from pathlib import Path

class BaseConfig:
    # *** Computation ***
    def root_path(self):
        return Path.home().joinpath("conrec_utils_workspace", "text2ssyk")
    
    def _get_ssyk(self, occId2ssyk, occId):
        if occId in occId2ssyk:
            ssyk = int(occId2ssyk[occId])
            level = self.use_ssyk_level()
            prod = 1
            for i in range(4-level):
                prod *= 10
            return int(ssyk/prod)
    
    def path_ssyk_data(self):
        return hf.project_root().joinpath("data")

    def path_source_data(self):
        return hf.project_root().joinpath("source_data")
    
    def path_data(self):
        return self.root_path().joinpath('data')
    
    def path_occ_id_2_ssyk(self):
        return self.path_ssyk_data().joinpath('occId_2_ssyk2012.json')

    def source_data_filenames(self):
        """Names of the files """
        pathData = self.path_source_data()
        return [pathData.joinpath(filename)
                for filename in os.listdir(pathData)
                if filename.lower().endswith(".json")]


    def sqlite_storage_path(self):
        """Path to the sqlite database where the jobads are stored for quick access"""
        return self.path_data().joinpath(f"jobads.db")

    def sqlite_storage(self):
        """Sqlite based storage of the dataset"""
        return SqliteStorage(self.sqlite_storage_path())
    
    def occ_id_2_ssyk(self):
        """Load the map from occupation id to SSYK"""
        with open(self.path_occ_id_2_ssyk()) as f:
            return json.load(f)

    def yrke_id_2_name(self):
        """Load the map from occupation id to name of occupation. *NOT IN USE*"""
        with open(self.path_ssyk_data().joinpath('occId_2_name.json')) as f:
            yrkeId2Name = json.load(f)

    def _import_dataset(self, storage):
        occId2ssyk = self.occ_id_2_ssyk()
        counter = 0
        def gen_index():
            nonlocal counter
            i = counter
            counter += 1
            return i
        for filename in self.source_data_filenames():
            items = (DatasetItem(gen_index(), ad["description"]["text"], ssyk)
                     for ad in read_json(filename)
                     for ssyk in [self._get_ssyk(occId2ssyk, occ_id_from_ad(ad))]
                     if ssyk is not None)
            storage.insert_or_update(items)

    def _import_dataset_if_needed(self):
        storage = self.sqlite_storage()
        if not(storage.exists()):
            self._import_dataset(storage)
        return storage

    def sample_indices_path(self, n):
        """This is where the subset used for training is stored"""
        return self.path_data().joinpath(f"sample_indicies_{n}.db")

    def nr_items(self):
        raise NotImplementedError("nr_items")
    
    def _get_or_generate_sample_inds(self, storage):
        n = self.nr_items()
        filename = self.sample_indices_path(n)
        if filename.exists():
            return read_json(filename)
        else:
            subset = random.sample(list(storage.get_indices()), n)
            write_json(filename, subset)
            return subset

    def dataset_handle(self):
        """Returns a context manager to the dataset (use in `with`-statement)"""
        storage = self._import_dataset_if_needed()
        inds = self._get_or_generate_sample_inds(storage)
        return DatasetHandle(storage, inds)    

    def unit_test_sample_count(self):
        return 100

    def unit_test_data_filename(self):
        return hf.project_root().joinpath("data", "unit_test_data.json")

    def load_data_for_unit_tests(self):
        return read_json(self.unit_test_data_filename())
    
    def generate_data_for_unit_tests(self):
        storage = self._import_dataset_if_needed()
        n = self.unit_test_sample_count()
        subset = random.sample(list(storage.get_indices()), n)
        assert(len(subset) == self.unit_test_sample_count())
        with DatasetHandle(storage, subset) as ds:
            write_json(self.unit_test_data_filename(),
                       [ds[i].dict() for i in range(n)])


            
            
            
