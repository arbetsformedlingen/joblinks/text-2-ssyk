# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import sys
import joblinks_classify

TEXT2SSYK_ROOT = joblinks_classify.model_dir_from_env()

def flatten_lists_into(dst, data):
    if isinstance(data, list):
        for x in data:
            flatten_lists_into(dst, x)
    else:
        dst.append(data)
    return dst

input_json = ''
#input_json = os.getenv('input_json')

data = []

for line in sys.stdin:
    try:
        data.append(json.loads(line))
    except:
        print('Could not load json from string:' + str(line), file=sys.stderr)
        exit(1)

if len(data) > 0:
    data = joblinks_classify.Text2SsykConfig.from_path(TEXT2SSYK_ROOT).load_and_classify(flatten_lists_into([], data))
else:
    print('No data input', file=sys.stderr)
    exit(1)

#os.environ['output_json'] = output_json

for d in data:
    print(json.dumps(d))
