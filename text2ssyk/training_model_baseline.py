# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import json
import numpy as np
import re
import nltk
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.corpus import stopwords
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
import pickle
import joblib
#from sklearn.externals import joblib
from pathlib import Path
import os
from sklearn.model_selection import StratifiedKFold
import text2ssyk.help_functions as hf
import random
from text2ssyk.data_handling import write_pickle, read_pickle, occ_id_from_ad, read_json, write_json, cached_evaluate
from text2ssyk.database import SqliteStorage, Dataset, DatasetItem, DatasetHandle, dataset_labels, dataset_texts
from text2ssyk.base_config import BaseConfig
from time import time

# *** How to use this code ***
#
# The `TrainingConfigBase` class contains the general code for training a
# text2ssyk classifier. You extend this class with your own class that
# implements specific settings, e.g. `TrainingConfig0`. Then you
# simply train the algorithm for that config by calling its `train_full` method:
#
# cfg = TrainingConfig0()
# cfg.train_full()
#
# If you want to perform a more rigorous run which includes cross validation,
# then do
#
# cfg.run_all()
#
# See the function `demo_train` below for a working example.



# *** Previously reported results ***
# Baseline model

# --------------------------------------------------
# Results 5-fold cross-validation (80/20 train/test)
# --------------------------------------------------
# 
# Classfier: Random forest
# Nr features: 15000
# 30 trees, max depth 100
#
# Nr items: 50 000
# -------------------------
# Score Exact: 58.2 +- 0.8
# Score in top 3: 71.8% +- 0.8
# Score in top 5: 75.7% +- 0.7
#
# Nr items: 100 000
# -------------------------
# Score Exact: 62.5% +- 0.3
# Score in top 3: 75.5% +- 0.2
# Score in top 5: 79.0% +- 0.2
#
# Nr items: 500 000
# -------------------------
# accuracy: 72.0 +- 0.1
# accuracy top 3: 82.5 +- 0.1
# accuracy top 5: 85.2 +- 0.1
#
# Nr items: 1M
# -------------------------
#exact: 76.2 % +- 0.1
#in top 3: 85.6 % +- 0.1
#in top 5: 87.8 % +- 0.1
#
#
# 20 trees, max depth 200:
# Nr items: 1M
# 
# exact: 78.0 % +- 0.04
# in top 3: 87.0 % +- 0.07
# in top 5: 89.0 % +- 0.1

# Nr y items:
# Occupation id / "yrkesbenamning": 
# SSYK Level 4: - (training data) / 429 (SCB)
# SSYK Level 3: - (training data) / 147 (SCB)
# SSYK Level 2: - (training data) / 46 (SCB)
# SSYK Level 1: - (training data) / 10 (SCB)
        
class TrainedModel:
    def __init__(self, vectorizer, tfidfconverter, classifier):
        self.vectorizer = vectorizer
        self.tfidfconverter = tfidfconverter
        self.classifier = classifier                
        
class TrainingConfigBase(BaseConfig):
    """This is a stateless configuration object for training the baseline algorithm. In order to customize the training and change parameters, create a class that inherits from this class and override the appropriate methods. To perform a full training including cross-validation, call the method `run_all`."""
    
    def model_name(self):
        """This is the name of the configuration. It is used to generate paths where the results are stored and retrieved."""
        raise NotImplementedError("model_name")

    def description(self):
        """This is a description of the configuration"""
        raise NotImplementedError("description")

    # *** Settings ***
    def classifier_type(self):
        """This is the type of classifier applied to the feature vectors"""
        return 'random_forest'

    def do_cross_validation(self):
        """Whether or not to do cross validation in run_all()"""
        return True

    def do_full_model(self):
        """Whether or not to train the full model in run_all()"""
        return True

    def use_ssyk_level(self):
        """Which ssyk level to classify"""
        return 4

    def nr_items(self):
        """Number of samples to pick when training and cross validating"""
        return 1300000

    def nr_features(self):
        """Number of features for the word vectors"""
        return 15000

    def crossval_splits(self):
        """The number of folds for cross validation"""
        return 5
    
    def rf_estimators(self):
        """Number of trees in the random forest"""
        return 20

    def rf_max_depth(self):
        """Max depth of a tree in the random forest"""
        return 300

    def tfid_df_min(self):
        """Minimum frequency threshold of tfidf vectors"""
        return 1

    def tfid_df_max(self):
        """Maximum frequency threhshold of tfidf vectors"""
        return 1.0
    
    def nr_parallel_jobs(self):
        """Amount of parallelism"""
        return 12

    def verbose_classifier(self):
        """Whether to display extra information while training the classifier"""
        return True

    def path_model(self):
        """Where to save the models"""
        raise NotImplementedError("path_model")

    def path_cache(self):
        """Where to cache things during training"""
        raise NotImplementedError("path_cache")
        
    def _build_count_vectorizer(self, x_all):
        print('vectorizer')
        min_df = self.tfid_df_min()
        max_df = self.tfid_df_max()
        nrFeatures = self.nr_features()
        vectorizer = CountVectorizer(
            max_features=nrFeatures, min_df=min_df, max_df=max_df,
            stop_words=stopwords.words('swedish'))
        x_all = vectorizer.fit_transform(x_all)
        return (vectorizer, x_all)
            
    def _build_tfid(self, x_all):
        print('tfid')
        tfidfconverter = TfidfTransformer()
        x_all = tfidfconverter.fit_transform(x_all)
        return (tfidfconverter, x_all)
    
    def _fit_transform_vec_tfid(self, x_all, cache_path): #,nrFeatures=10000
        (vectorizer, x_all) = cached_evaluate(cache_path, "vectorizer.pickle", lambda: self._build_count_vectorizer(x_all))
        (tfidfconverter, x_all) = cached_evaluate(cache_path, "tfidf.pickle", lambda: self._build_tfid(x_all))
        return [x_all, vectorizer, tfidfconverter]

    def _accuracy_score_topX(self, y_test,resTop):
        nrCorrectTopX = 0
        for i,c in enumerate(y_test):
            if c in resTop[i]:
                nrCorrectTopX+=1

        return float(nrCorrectTopX)/len(y_test)

    def initialize_classifier(self):
        """This instantiates an untrained classifier."""
        nrFeatures = self.nr_features()
        rf_estimators = self.rf_estimators()
        rf_max_depth = self.rf_max_depth()
        nr_par = self.nr_parallel_jobs()
        verbose = self.verbose_classifier()
        classType = self.classifier_type()
        print('classifier:',classType)
        
        if classType == 'random_forest':
            return RandomForestClassifier(
                n_estimators=rf_estimators,
                random_state=0,
                max_depth = rf_max_depth,
                n_jobs=nr_par, verbose=verbose)
        elif classType == 'naive_bayes':
            return MultinomialNB()
        elif classType == 'knn':
            return KNeighborsClassifier(n_neighbors=3, n_jobs=nr_par)
    
    def iteration_train(self, dataset, cache_path): #rf_estimators = 100, rf_max_depth = 30
        """This will train the baseline model on preprocessed data and return the trained model"""
        X_train = dataset_texts(dataset)
        y_train = dataset_labels(dataset)
        print('train',len(X_train),'samples')

        [X_train, vectorizer,tfidfconverter] = self._fit_transform_vec_tfid(X_train, cache_path)

        start = time()
        classifier = self.initialize_classifier()
        classifier.fit(X_train, y_train)
        print(f"Trained classifier in {time() - start} seconds.")

        print('completed')

        return TrainedModel(vectorizer, tfidfconverter, classifier)

    def train_full(self):
        """This trains a model on the full dataset and saves it."""
        print(f"Train full: {self.model_name()}")
        with self.dataset_handle() as dataset:
            self.save_trained_model(self.iteration_train(dataset, cache_path=self.path_cache()))

    def iteration_test(self, dataset, trained_model):
        """This tests a trained model on a dataset"""
        vectorizer = trained_model.vectorizer
        tfidfconverter = trained_model.tfidfconverter
        classifier = trained_model.classifier
        X_test = dataset_texts(dataset)
        y_test = dataset_labels(dataset)

        print('test',len(X_test),'samples')

        X_test = vectorizer.transform(X_test)
        X_test = tfidfconverter.transform(X_test)

        y_pred = classifier.predict(X_test)
        y_predTop = classifier.predict_proba(X_test)

        rTop5 = np.argsort(y_predTop)[:,:-5-1:-1]
        resTop3 = []
        resTop5 = []

        for r in rTop5:
            rs = [classifier.classes_[i] for i in r]
            resTop3.append(rs[0:3])
            resTop5.append(rs)

        score = accuracy_score(y_test, y_pred)
        scoreTop3 = self._accuracy_score_topX(y_test,resTop3)
        scoreTop5 = self._accuracy_score_topX(y_test,resTop5)

        print('accuracy:', score)
        print('accuracy top 3:', scoreTop3)
        print('accuracy top 5:', scoreTop5)

        return [score, scoreTop3, scoreTop5]

    def perform_cross_validation(self, dataset):
        """This performs cross validation and reports the results to the standard output."""
        
        skf = StratifiedKFold(n_splits=self.crossval_splits())
        index = 0

        allres = []
        allresTop3 = []
        allresTop5 = []

        X = dataset_texts(dataset)
        y = dataset_labels(dataset)

        for train_index, test_index in skf.split(X, y):
            print('index:',index)
            trained_model = self.iteration_train(dataset[train_index], cache_path=None)
            [score, score_top3, score_top5] = self.iteration_test(dataset[test_index], trained_model)
            allres.append(score)
            allresTop3.append(score_top3)
            allresTop5.append(score_top5)
            index+=1
        print('results cross-validation')

        print('exact:',100.0*np.mean(allres),'% +-',100.0*np.std(allres))
        print('in top 3:',100.0*np.mean(allresTop3),'% +-',100.0*np.std(allresTop3))
        print('in top 5:',100.0*np.mean(allresTop5),'% +-',100.0*np.std(allresTop5))


    def vectorizer_filename(self):
        """This is the path to the vectorizer"""
        return self.path_model().joinpath(f"vector_{self.nr_items()}_{self.nr_features()}.pickle")

    def tfid_filename(self):
        """This is the path to the tfidf analysis"""
        return self.path_model().joinpath(f"tfid_{self.nr_items()}_{self.nr_features()}.pickle")

    def classifier_filename(self):
        """This is the path to the trained classifier"""
        return self.path_model().joinpath(
            f"model_{self.classifier_type()}_{self.nr_items()}_{self.nr_features()}_{self.rf_max_depth()}.gzip")
        
    def save_trained_model(self, trained_model):
        """This saves the trained model (vectorizer, tfidf and classifier)"""
        os.makedirs(self.path_model(), exist_ok=True)
        joblib.dump(value=trained_model.classifier, filename=self.classifier_filename(), compress=True)
        write_pickle(self.vectorizer_filename(), trained_model.vectorizer)
        write_pickle(self.tfid_filename(), trained_model.tfidfconverter)
        print(f"Trained model saved to '{self.path_model()}'")

    def run_all(self):
        """This method performs cross validation and trains a model on the full dataset."""
        print("Prepare data")
        with self.dataset_handle() as dataset:
            if self.do_cross_validation():
                print("Cross validation")
                self.perform_cross_validation(dataset)
            if self.do_full_model():
                print("Train full model")
                trained_model = self.iteration_train(dataset, cache_path=self.path_cache())
                self.save_trained_model(trained_model)
        print('Done')




class DefaultTrainingConfig(TrainingConfigBase):
    """This class extends TrainingConfigBase with sensible defaults."""
    def path_model(self):
        return self.root_path().joinpath('trained_models', self.model_name())

    def path_cache(self):
        # Don't do caching. Otherwise, do self.path_model().joinpath("cache")
        return None 

class TrainingConfig0(DefaultTrainingConfig):
    def description(self):
        return """This class represents the training
        configuration of what has currently been used
        in production to April 2023."""
    
    def nr_items(self):
        return 300000

    def rf_estimators(self):
        return 30

    def rf_max_depth(self):
        return 100
    
    def model_name(self):
        return "config0"
    
class TrainingConfig1(DefaultTrainingConfig):
    def description(self):
        return """These settings are more effective
        than TrainingConfig0 and seem
        to yield better results."""
    
    def nr_items(self):
        return 2000000

    def rf_estimators(self):
        return 20

    def rf_max_depth(self):
        return 200
    
    def model_name(self):
        return "config1"
    


def main():
    TrainingConfig1().train_full()



## DEMO

class TrainingConfig_DEMO(DefaultTrainingConfig):
    def description(self):
        return """This configuration only serves to demonstrate how you do
        the training and has been configured for a small dataset to keep
        training time short. You can use it during development to test
        that the code works."""
    
    def nr_items(self):
        return 20000

    def rf_estimators(self):
        return 30

    def rf_max_depth(self):
        return 100
    
    def model_name(self):
        return "config_demo"


def demo_train():
    """Run this function to try out the training yourself."""

    cfg = TrainingConfig_DEMO()

    print(f"Train {cfg.model_name()}: '{cfg.description()}'")
    print(f"The training algorithm expects the following files to be present:")
    for p in [p
              for group in [cfg.source_data_filenames(), [cfg.path_occ_id_2_ssyk()]]
              for p in group]:
        print(f"  * {p}")


    # Do the training
    cfg.run_all()

