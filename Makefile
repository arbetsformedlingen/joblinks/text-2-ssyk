.PHONY: models integration-test build-docker-image run-docker-example train-baseline train-baseline-demo train-enrich train-vectors install install_poetry install_stopwords download-source-data-files
SHELL = /bin/bash

docker_exec = podman
source_data_dir = source_data
dev_model_dir = models/dev
prod_model_dir = models/prod
dev_models = $(dev_model_dir)/model_random_forest_300000_15000.gzip $(dev_model_dir)/vector_300000_15000.pickle $(dev_model_dir)/tfid_300000_15000.pickle
prod_models = $(prod_model_dir)/model_random_forest_2000000_15000_200.gzip  $(prod_model_dir)/tfid_2000000_15000.pickle $(prod_model_dir)/vector_2000000_15000.pickle
all_models = $(dev_models) $(prod_models)
source_data_files = $(source_data_dir)/2018.json $(source_data_dir)/2019.json

download-source-data-files: $(source_data_files)

$(all_models):
	git lfs pull

source_data/%.json:
	curl --retry 5 --retry-max-time 120 -o $@ https://data.jobtechdev.se/annonser/historiska/$*.json

download-models: $(all_models)

build-docker-image: requirements.txt
	$(docker_exec) build -t text2ssyk .

run-docker-example:
	echo '{"originalJobPosting": { "description":"baka pizza" }}' | $(docker_exec) run -i text2ssyk

install: install_poetry install_stopwords

install_poetry:
	poetry install

install_stopwords:
	poetry run python <(echo -e "import nltk\nnltk.download('stopwords')")

run-stdin: $(dev_models)
	poetry run python3 text2ssyk/joblinks_main.py

run-example: $(dev_models)
	echo '[{"originalJobPosting": { "description":"baka pizza" }},{"originalJobPosting": { "description":"städa kontor"}}]' | make run-stdin

test: $(dev_models)
	poetry run python3 -m unittest discover --start-directory tests

specific-test:
	poetry run python3 -m unittest tests.test_database

integration-test: test requirements.txt

requirements.txt: pyproject.toml
	poetry export -f requirements.txt --output requirements.txt

# Training

train-baseline:
	poetry run python3 -c 'from text2ssyk.training_model_baseline import main; main()'

train-baseline-demo: $(source_data_files)
	poetry run python3 -c 'from text2ssyk.training_model_baseline import demo_train; demo_train()'

train-enrich:
	poetry run python3 text2ssyk/training_model_enrich.py

train-vectors:
	poetry run python3 text2ssyk/training_model_vectors.py
