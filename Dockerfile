FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /app

COPY run.sh requirements.txt dl_prod_models.sh /app/
COPY text2ssyk /app/text2ssyk

RUN apt -y update \
    && apt -y install curl python3 python3-pip \
    && pip install -r requirements.txt \
    && pip list \
    && chmod g+r /app/*

ENV PYTHONPATH=/app

## IMPORTANT: When models are updated, also update this download link:
ENV MODEL_DL_DIR=https://gitlab.com/joblinks/text-2-ssyk/-/raw/9bf160e919fd74e0f762467df5cd036c446b89c0



###############################################################################
FROM base AS test

COPY tests /app/tests
COPY models/dev /app/models/dev
COPY pyproject.toml /app
COPY data /app/data

RUN bash tests/run_container_test.sh /app/models/dev /app \
    && touch /.tests-successful



###############################################################################
FROM base

COPY --from=test /.tests-successful /

ENV TEXT2SSYK_ROOT=/app

CMD ["/bin/bash", "-c", "sh ./dl_prod_models.sh && python3 text2ssyk/joblinks_main.py"]
