# Copyright 2022 Arbetsförmedlingen JobTech.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
import text2ssyk.joblinks_classify as jlc
from subprocess import Popen
import subprocess
import json
from text2ssyk.base_config import BaseConfig

def write_str(buf, s):
    buf.write(s.encode('utf-8'))

class TestText2Ssyk(unittest.TestCase):
    def test_classify(self):
        cfg = jlc.Text2SsykConfig.from_path(jlc.dev_model_dir)
        result = cfg.load_and_classify([
            {"originalJobPosting": {"description": "baka pizza"}},
            {"originalJobPosting": {"description": "städa kontor"}}
        ])
        self.assertEqual(result[0]["ssyk_lvl4"], 9411)
        self.assertEqual(result[1]["ssyk_lvl4"], 9111)

        classifier = cfg.load_classifier()
        top3 = classifier.classifyTextsTopN(["baka pizza", "städa kontor"], 3)

        # Two strings to classify
        self.assertEqual(2, len(top3))

        # The top 3 candidates for each string
        self.assertEqual(3, len(top3[0]))
        self.assertEqual(3, len(top3[1]))

        # Check the best candidate of each input string
        self.assertEqual(9411, top3[0][0])
        self.assertEqual(9111, top3[1][0])

    def test_full_pipeline(self):
        with Popen(['python3',
                    'text2ssyk/joblinks_main.py'],
                   stdout=subprocess.PIPE, stdin=subprocess.PIPE) as proc:

            write_str(
                proc.stdin,
                '{"id": 119, "originalJobPosting": { "description":"baka pizza" }}\n')
            write_str(
                proc.stdin,
                '{"id": 120, "originalJobPosting": { "description":"städa kontor"}}\n')
            proc.stdin.close()

            output = []
            for line in proc.stdout:
                output.append(json.loads(line.decode('utf-8')))

            expected = [{'id': 119, 'originalJobPosting': {'description': 'baka pizza'}, 'ssyk_lvl4': 9411},
                        {'id': 120, 'originalJobPosting': {'description': 'städa kontor'}, 'ssyk_lvl4': 9111}]

            self.assertEqual(output, expected)

    def test_accuracy(self):
        cfg = jlc.Text2SsykConfig.from_path(jlc.model_dir_from_env())
        samples = BaseConfig().load_data_for_unit_tests()

        total_count = len(samples)

        classifier_input = [{"originalJobPosting": {"description": sample["text"]}} for sample in samples]

        print(f"Classifying {total_count} samples")
        classifier_output = cfg.load_and_classify(classifier_input)

        correct_count = 0
        for (sample, result) in zip(samples, classifier_output):
            if sample["label"] == result["ssyk_lvl4"]:
                correct_count += 1

        accuracy = correct_count/total_count
        print(f"Accuracy: {accuracy}")
        self.assertLess(0.5, accuracy)
