#!/bin/bash
set -euo pipefail
IFS=$'\n\t'


TEXT2SSYK_ROOT="${1:-$PWD/models/dev}"
PROJ_DIR="${2:-$PWD}"

export TEXT2SSYK_ROOT

pip install requests==2.31.0

if [ ! -d "$TEXT2SSYK_ROOT" ]; then
    mkdir -p "$TEXT2SSYK_ROOT"

    cd "$TEXT2SSYK_ROOT"
    echo "INFO: downloading models to $TEXT2SSYK_ROOT" >&2
    bash "$PROJ_DIR"/dl_prod_models.sh
else
    echo "INFO: $TEXT2SSYK_ROOT exists, not downloading models" >&2
fi


cd "$PROJ_DIR"


if [ ! -d data ]; then
    if [ -n "$CI_PROJECT_URL" ] && [ -n "$CI_COMMIT_REF_NAME" ]; then
        URL="$CI_PROJECT_URL/-/archive/$CI_COMMIT_REF_NAME/text-2-ssyk-master.tar.gz?path=data"
    else
        ARCHIVE_URL="${MODEL_DL_DIR/\/raw\//\/archive\/}"
        URL="$ARCHIVE_URL/text-2-ssyk-master.tar.gz?path=data"
    fi
    echo "INFO: test uses data/ from $URL" >&2
    curl -L "$URL" | tar -zxf -
else
    echo "INFO: data exists, not downloading" >&2
fi

echo "INFO: running unit tests" >&2
python3 -m unittest discover --start-directory tests


echo "INFO: running 10 ads" >&2
python3 text2ssyk/joblinks_main.py < tests/testdata/input.10 > output.10


echo "INFO: comparing the output with the expected output" >&2
diff tests/testdata/expected_output.10 output.10
