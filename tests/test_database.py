import text2ssyk.database as db
import unittest
import tempfile
from pathlib import Path

class TestDatabase(unittest.TestCase):
    def test_storage(self):
        with tempfile.TemporaryDirectory() as tempdir:
            p = Path(tempdir).joinpath("data.db")
            storage = db.SqliteStorage(p)
            self.assertFalse(storage.exists())
            items = [db.DatasetItem(3, "städerska", 345),
                     db.DatasetItem(4, "målare", 9),
                     db.DatasetItem(5, "2019", 34)]


            self.assertNotEqual(items[0], items[1])
            
            storage.insert_or_update(items)
            self.assertTrue(storage.exists())

            self.assertEqual({3, 4, 5}, set(storage.get_indices()))

            with storage.random_access() as s:
                for x in items:
                    y = s[x.index]
                    self.assertEqual(x, y)

                ds = db.Dataset(s, storage.get_indices())
                self.assertEqual(3, len(ds))
                self.assertIn(ds[0].index, {3, 4})
                self.assertIn(ds[1].index, {3, 4})
                self.assertNotEqual(ds[0], ds[1])

                sliced_ds = ds[[1]]
                self.assertEqual(ds[1], sliced_ds[0])

                ds_labels = ds.map(lambda x: x.label)

                for i in range(3):
                    self.assertEqual(ds[i].label, ds_labels[i])

            with db.DatasetHandle(storage, [3, 5]) as dataset:
                self.assertEqual(2, len(dataset))
                self.assertEqual(items[0], dataset[0])
                self.assertEqual(items[2], dataset[1])                

                
                    
