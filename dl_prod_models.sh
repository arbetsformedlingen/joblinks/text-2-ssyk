#!/bin/bash
set -euo pipefail

MODEL_DL_DIR=${MODEL_DL_DIR:-1}

if [ -z $MODEL_DL_DIR ]; then
    echo "ERROR: something is wrong - missing MODEL_DL_DIR" >&2
    exit 1
fi


for F in model_random_forest_2000000_15000_200.gzip vector_2000000_15000.pickle tfid_2000000_15000.pickle; do
    if [ ! -f "$F" ]; then
        echo "INFO: downloading $MODEL_DL_DIR/models/prod/$F" >&2
        curl -L -O "$MODEL_DL_DIR"/models/prod/"$F"
    else
        echo "INFO: $F exists, not downloading" >&2
    fi
done
